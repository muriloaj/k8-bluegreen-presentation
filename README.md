# Bluegreen with Kubernetes - simple tutorial - 



Simple tutorial to show how simple can be migrate a docker-compose project into a kubernetes(k8s) structure and use the k8s to manage a blue-green deployment.



To reproduce it, please, install a microk8s, or other federated kubernetes tool, as minikube, or use the cloud providers, as weel. The link of the main tools , repository and video explaining how it works are bellow.



Thank you for reading and watching this asciicinema.


Git repository: https://bitbucket.org/muriloaj/k8-bluegreen-presentation

Video (Asciicinema) : https://asciinema.org/a/CWmy048CjofdmQ0rwKrAVz7X1



extra links:

- kompose ->  http://kompose.io/

- microk8s -> https://microk8s.io/

